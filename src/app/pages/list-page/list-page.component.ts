import { Component, OnInit, Input } from '@angular/core';
import { RecipesListService } from '../../services/recipes-list.service';
import { RecipesListModel } from '../../models/recipes-list.model';

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.css'],
  providers: [RecipesListService]
})
export class ListPageComponent implements OnInit {
  public searchByNameEnabled: boolean = false;
  public searchByIngredientsEnabled: boolean = true;
  public recipes: RecipesListModel[];

  constructor(private recipeService: RecipesListService) { 
    this.recipeService.getFilteresRecipesList(null)
    .subscribe(res => {
      this.recipes = res.map(recipe =>
        ({
          Id: recipe.Id,
          Name: recipe.Name,
          Photo: 'data:image/png;base64,' + recipe.Photo,
          Category: recipe.Category,
          UserId: recipe.UserId,
          RatingValue: recipe.RatingValue
        }));
        this.recipes = this.recipes.slice(0);
        this.recipes.sort((left, right): number =>{
          if(left.RatingValue > right.RatingValue) return -1;
          if(left.RatingValue < right.RatingValue) return 1;
          return 0;
        })
    });
  }

  ngOnInit() {
   
  }

  enableSearchByName(){
    this.searchByNameEnabled = true;
    this.searchByIngredientsEnabled = false;
  }

  enableSearchByIngredients(){
    this.searchByNameEnabled = false;
    this.searchByIngredientsEnabled = true;
  }
}
