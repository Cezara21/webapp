import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import { MenuModule } from 'primeng/components/menu/menu';
import { DataGridModule, ButtonModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/panel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabViewModule } from 'primeng/tabview';
import { ChartModule } from 'primeng/chart';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CheckboxModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import { Button } from 'primeng/button';
import { FileUploadModule } from 'primeng/fileupload';
import { GrowlModule } from 'primeng/growl';
import { RatingModule } from 'primeng/rating';
import { DialogModule } from 'primeng/dialog';
 
import { AppComponent } from './app.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { RecipesListComponent } from './components/list-page/recipes-list/recipes-list.component';
import { RecipeDetailsComponent } from './components/recipe-page/recipe-details/recipe-details.component';
import { RecipeNutrientsComponent } from './components/recipe-page/recipe-nutrients/recipe-nutrients.component';
import { ListPageComponent } from './pages/list-page/list-page.component';
import { RecipePageComponent } from './pages/recipe-page/recipe-page.component';
import { TablePartComponent } from './components/recipe-page/table-part/table-part.component';
import { TableComponent } from './components/recipe-page/table/table.component';
import { SearchByIngredientsComponent } from './components/list-page/recipes-search/search-by-ingredients/search-by-ingredients.component';
import { AddRecipeComponent } from './components/user-menu/add-recipe/add-recipe.component';
import { RegisterComponent } from './components/users/register/register.component';
import { JwtInterceptor } from './services/interceptors/jwtInterceptor';
import { LoginComponent } from './components/users/login/login.component';
import { UsersService } from './services/users.service';
import { SearchByNameComponent}  from './components/list-page/recipes-search/search-by-name/search-by-name.component';
import { FavoritesComponent } from './components/user-menu/favorites/favorites.component';
import { UserRecipesComponent } from './components/user-menu/user-recipes/user-recipes.component';
import { RoundPipe } from './helpers/round.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    RecipesListComponent,
    RecipeDetailsComponent,
    RecipeNutrientsComponent,
    ListPageComponent,
    RecipePageComponent,
    TablePartComponent,
    TableComponent,
    SearchByIngredientsComponent,
    AddRecipeComponent,
    RegisterComponent,
    LoginComponent,
    SearchByNameComponent,
    FavoritesComponent,
    UserRecipesComponent,
    RoundPipe
  ],
  imports: [
    BrowserModule,
    MenuModule,
    HttpClientModule, 
    DataGridModule,
    PanelModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TabViewModule,
    ChartModule,
    AutoCompleteModule,
    FormsModule,
    CheckboxModule,
    InputTextModule,
    InputTextareaModule,
    DropdownModule,
    ButtonModule,
    FileUploadModule,
    ReactiveFormsModule,
    GrowlModule,
    RatingModule,
    DialogModule
  ],
  providers: [ UsersService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
