import { NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { RecipePageComponent } from './pages/recipe-page/recipe-page.component';
import { ListPageComponent } from './pages/list-page/list-page.component';
import { AppComponent } from './app.component';
import { AddRecipeComponent } from './components/user-menu/add-recipe/add-recipe.component';
import { RegisterComponent } from './components/users/register/register.component';
import { LoginComponent } from './components/users/login/login.component';
import { UserloggedInGuardGuard } from './userlogged-in-guard';
import { FavoritesComponent } from './components/user-menu/favorites/favorites.component';
import { UserRecipesComponent } from './components/user-menu/user-recipes/user-recipes.component';

const appRoutes: Routes = [
    {path: '', component: ListPageComponent},
    {path: 'recipe-details/:id', component: RecipePageComponent},
    {path: 'recipe-list', component: ListPageComponent },
    {path: 'add-recipe', component: AddRecipeComponent}, // ,canActivate: [UserloggedInGuardGuard]},
    {path: 'app-register', component: RegisterComponent},
    {path: 'app-login', component: LoginComponent},
    {path: 'app-favorites', component: FavoritesComponent},
    {path: 'app-user-recipes', component: UserRecipesComponent}
  ];

  @NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule],
    providers: [UserloggedInGuardGuard]
  })
  export class AppRoutingModule { }