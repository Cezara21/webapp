export class Diacritics{
    
    static normaliseString(ingredient: string) : string{
        ingredient = ingredient.replace('ș', 's').replace('ă', 'a')
            .replace('ț', 't').replace('î', 'i').replace('â', 'a');
        return ingredient;
    }
}