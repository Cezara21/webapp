import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { RouterLink, NavigationStart, Router } from '@angular/router';
import { UsersService } from '../services/users.service';


@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css'],
  providers: [UsersService]
})
export class NavigationBarComponent implements OnInit, OnChanges{
  
  username: string = "";
  userMenuEnabled:boolean = false;
  isLoggedIn:boolean;

  constructor(private router: Router, private usersService: UsersService) { 
    this.router.events.subscribe((res)=>{
      this.isLoggedIn = this.usersService.getUser()!=null;
      this.username = this.usersService.getUserName();  
    });
  }

  ngOnInit() {
    this.username = this.usersService.getUserName();
    this.isLoggedIn = this.username != null;
  }

  ngOnChanges(changes: SimpleChanges): void {
    // if(changes.isLoggedIn){
    //   this.isLoggedIn = this.isLoggedIn;
    // }
  }

  enableUserMenu(){
    this.userMenuEnabled = true;
  }

  disableUserMenu(){
    this.userMenuEnabled = false;
  }

  logout(){
    this.usersService.logout();
    this.isLoggedIn = false;
    this.router.navigate(['/recipe-list']);
  }
}
