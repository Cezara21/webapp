export class RatingInfo { 
    UserId: string;
    RecipeId: number;
    Value: number;
}