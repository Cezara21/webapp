export class CustomIngredientInfo{
        public Ingredient: string;
        public Measure: string;
        public Quantity: number;
}