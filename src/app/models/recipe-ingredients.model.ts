export class RecipeIngredientsModel{
    public IngredientId: number;
    public RecipeId: number;
    public IngredientName: string;
    public MeasureName: string;
    public Quantity: number;
}
