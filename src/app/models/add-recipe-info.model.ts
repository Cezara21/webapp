import { CustomIngredientInfo } from "./custom-ingredient-info.model";

export class AddRecipeInfo {
        public Title: string;
        public Category: string;
        public CustomIngredients: CustomIngredientInfo[];
        public Directions: string;
        public Photo: string;
        public UserId: string;
        constructor(){
                this.CustomIngredients = [];
        }
}