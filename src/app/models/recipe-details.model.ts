export class RecipeDetailsModel {
    public Id: number;
    public Name: string;
    public Directions: string;
    public Steps: string[];
    public Photo: string;
    public RatingValue: number;
}