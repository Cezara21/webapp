export class RecipesListModel{
    public Id: number;
    public Name: string;
    public Photo: string;
    public Category: string;
    public UserId: string;
    public RatingValue: number;
}