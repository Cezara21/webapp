export class RecipeNutrientValue{
    constructor(
        public Name: string,
        public Value: number,
        public Measure: string,
        public Category: string,
        public PercentFromDRI: number
    ){}
}