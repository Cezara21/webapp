import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { DataTable } from 'primeng/primeng';
import "rxjs/add/operator/toPromise";
import 'rxjs/add/operator/map'
import { RecipesListModel } from "../models/recipes-list.model";
import { BaseService } from './base.service';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { RecipeDetailsModel } from '../models/recipe-details.model';
import { RecipeIngredientsModel } from '../models/recipe-ingredients.model';
import { Ingredients } from '../models/ingredients.model';
import { RecipeNutrientValue } from '../models/recipe-nutrient-value';
import { AddRecipeInfo } from '../models/add-recipe-info.model';
import { UserFavoriteInfo } from '../models/user-favorite-info-model';
import { RatingInfo } from '../models/rating-info-model';
import { Subject } from 'rxjs';
import { IngredientMeasure } from '../models/ingredient-measure-model';



@Injectable()
export class RecipesListService extends BaseService{

  constructor(private httpClient: HttpClient) {
    super();
  }
  /*  this.getFilteresRecipesList(this.ingredients)
    .subscribe(res => {
      this.recipes = res.map(recipe =>
        ({
          Id: recipe.Id,
          Name: recipe.Name,
          Photo: 'data:image/png;base64,' + recipe.Photo,
          Category: recipe.Category,
          UserId: recipe.UserId
        }));
      console.log("raspuns" + res.length);
    });
    console.log("service "+this.recipes);
  }

  getRecipeList(): RecipesListModel[]{
    return this.recipes.asObservable();
  }*/

  getRecipesList(): Observable<RecipesListModel[]>{
    const requestUrl = "api/recipes";

    return this.httpClient.get<RecipesListModel[]>(this.baseUrl + requestUrl, {headers: this.headers});
  }

  getRecipeCategories(): Observable<string[]>{
    const requestUrl = "api/recipes/categories";

    return this.httpClient.get<string[]>(this.baseUrl + requestUrl);
  }

  getIngredientMeasures(): Observable<string[]>{
    const requestUrl = "ingredients/measures";

    return this.httpClient.get<string[]>(this.baseUrl + requestUrl);
  }

  getIngredientsCustomMeasures():Observable<IngredientMeasure[]>{
    const requestUrl = "ingredients/measures/custom";

    return this.httpClient.get<IngredientMeasure[]>(this.baseUrl + requestUrl);
  }

  getFilteresRecipesList(ingredients:string[]):Observable<RecipesListModel[]>{
    const requestUrl = "api/recipes/ingredients";
    const body = ingredients;

    return this.httpClient.post<RecipesListModel[]>(this.baseUrl + requestUrl, body,  {headers: this.headers});
  }

  postRecipe(recipe: AddRecipeInfo): Observable<number>{
    const requestUrl = "api/recipes";
    const body = recipe;

    return this.httpClient.post<number>(this.baseUrl + requestUrl, body, {headers: this.headers});
  }

  postUserFavorite(userFavorite: UserFavoriteInfo): Observable<number>{
    const requestUrl = "api/recipes/favorites";
    const body = userFavorite;

    return this.httpClient.post<number>(this.baseUrl + requestUrl, body, {headers: this.headers});
  }

  deleteUserFavorite(userId:string, recipeId:number): Observable<number>{
    const requestUrl = `api/recipes/${recipeId}/${userId}`;

    return this.httpClient.delete<number>(this.baseUrl + requestUrl, {headers: this.headers});
  }

  sendRating(rating : RatingInfo):Observable<number>{
    const requestUrl = "api/recipes/rating";
    const body = rating;
    console.log(rating);

    return this.httpClient.post<number>(this.baseUrl + requestUrl, body, {headers: this.headers});
  }

  getUserRating(userId:string, recipeId:number): Observable<number>{
    const requestUrl = `api/recipes/${recipeId}/${userId}`;

    return this.httpClient.get<number>(this.baseUrl + requestUrl);
  }

  getUserFavorites(userId: string) : Observable<RecipesListModel[]>{
    const requestUrl = `api/recipes/${userId}/favorites`;
    
    return this.httpClient.get<RecipesListModel[]>(this.baseUrl + requestUrl);
  }

  getRecipeDetails(recipeId): Observable<RecipeDetailsModel>{
    const requestUrl= `api/recipes/${recipeId}/details`;

    return this.httpClient.get<RecipeDetailsModel>(this.baseUrl + requestUrl);
  }

  getRecipeIngredients(recipeId) : Observable<RecipeIngredientsModel[]>{
    const requestUrl = `ingredients/${recipeId}`;

    return this.httpClient.get<RecipeIngredientsModel[]>(this.baseUrl + requestUrl);
  }
  
  getAllIngredients(): Observable<string[]>{
    const requestUrl = `ingredients`;

    return this.httpClient.get<string[]>(this.baseUrl + requestUrl);
  }

  getFullNameIngredients(): Observable<string[]>{
    const requestUrl = 'ingredients/fullname'; 

    return this.httpClient.get<string[]>(this.baseUrl + requestUrl);
  }

  getRecipesNames():Observable<string[]>{
    const requestUrl = 'api/recipes/names';

    return this.httpClient.get<string[]>(this.baseUrl + requestUrl);
  }

  getNutrientValues(recipeId): Observable<RecipeNutrientValue[]>{
    const requestUrl = `api/recipes/${recipeId}/nutritionalValues`;

    return this.httpClient.get<RecipeNutrientValue[]>(this.baseUrl + requestUrl);
  }

  private handleError(error:any):Promise<any> {
    console.error('An error ocurred', error);
    return Promise.reject(error.message || error);
  }
  
}
