import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
 
@Injectable()
export class BaseService {
    public baseUrl="http://localhost:52386/";
    public headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json');
}
