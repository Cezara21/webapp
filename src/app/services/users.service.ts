import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { User } from '../models/user.model';
import { Observable } from 'rxjs/Observable';
import { BaseService } from './base.service';
import { Subscription } from 'rxjs';

@Injectable()
export class UsersService extends BaseService{

  constructor(private httpClient: HttpClient) {
    super();
   }

  private actions = {
    register : "api/user/register",
    login: "api/user/login"
  };

  register(user: User): Observable<any>{
    return this.httpClient.post<any>(this.baseUrl + this.actions.register, user);
  }

  login(email: string, password: string) {
    return this.httpClient.post<any>(this.baseUrl + this.actions.login, { email, password });
  }

  getUser(){
    var user: any;

    user = JSON.parse(localStorage.getItem('currentUser'));
    return user;
  }

  getUserEmail() {
    var user = this.getUser();
    if(user==null)
    {
      return null;
    }
    return (user.Email);
  }

  getUserId() {
    var user = this.getUser();
    if(user==null)
    {
      return null;
    }
    return (user.Id);
  }

  getUserName(){
    var user = this.getUser();
    if(user==null)
    {
      return null;
    }
    return (user.Name);
  }

  logout() {
    localStorage.removeItem('currentUser');
  }
}
