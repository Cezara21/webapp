import { TestBed, inject } from '@angular/core/testing';

import { RecipesListService } from './recipes-list.service';

describe('RecipesListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecipesListService]
    });
  });

  it('should be created', inject([RecipesListService], (service: RecipesListService) => {
    expect(service).toBeTruthy();
  }));
});
