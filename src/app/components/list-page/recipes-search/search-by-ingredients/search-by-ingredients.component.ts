import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RecipesListService } from '../../../../services/recipes-list.service';
import { Diacritics } from '../../../../helpers/diacritics';


@Component({
  selector: 'app-search-by-ingredients',
  templateUrl: './search-by-ingredients.component.html',
  styleUrls: ['./search-by-ingredients.component.css'],
  providers: [RecipesListService]
})
export class SearchByIngredientsComponent implements OnInit {
  public ingredientToRemove: string;
  public inputIngredients: string[] = [];
  public filteredIngredients: string[];
  public ingredients: string[];
  public emptyMessage: string = "Nu exista reteta cu acest ingredient";
  
  @Output() onSearch = new EventEmitter<string[]>();

  constructor(private recipeService : RecipesListService) { 
    this.recipeService.getAllIngredients().subscribe(res=>
    this.ingredients=res);
  }

  ngOnInit() {
  }


  filterIngredientsMultiple(event){
    let query=event.query;
    this.filteredIngredients = this.filterIngredients(query, this.ingredients);
  }

  filterIngredients(query, ingredients: string[]):any[]{
    let filtered : any[] = [];
    for(let i=0; i<ingredients.length;i++)
    {
      let ingredient = ingredients[i];
      let normalisedIngredient = Diacritics.normaliseString(ingredient); 
      if(normalisedIngredient.includes(query.toLowerCase())){
        filtered.push(ingredient);
      }
    }
    console.log(filtered);
    return filtered;
  }

  removeIngredient(event){
    this.ingredientToRemove = event;
    const index:number = this.inputIngredients.indexOf(this.ingredientToRemove);
    delete this.inputIngredients[index];
    this.search();
  }

  search(){
    this.onSearch.emit(this.inputIngredients);
  }
}
