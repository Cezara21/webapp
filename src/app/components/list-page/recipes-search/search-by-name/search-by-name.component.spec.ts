import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipesSearchBynameComponent } from './recipes-search-byname.component';

describe('RecipesSearchBynameComponent', () => {
  let component: RecipesSearchBynameComponent;
  let fixture: ComponentFixture<RecipesSearchBynameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipesSearchBynameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipesSearchBynameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
