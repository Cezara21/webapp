import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { RecipesListService } from '../../../../services/recipes-list.service';
import { RecipesListModel } from '../../../../models/recipes-list.model';
import { Router } from '@angular/router';
import { Diacritics } from '../../../../helpers/diacritics';

@Component({
  selector: 'app-search-by-name',
  templateUrl: './search-by-name.component.html',
  styleUrls: ['./search-by-name.component.css'],
  providers: [RecipesListService]
})
export class SearchByNameComponent implements OnInit, OnChanges {
  public recipeName: string="";
  public recipes : RecipesListModel[] = [];
  public filteredRecipeNames: string[] = [];
  public recipesNumber : number = 0;
  public recipesNames: string[] = [];
  public emptyMessage: string = "Nu exista aceasta reteta";

  @Input() inputRecipes: RecipesListModel[];

  constructor(private recipeService : RecipesListService, private router: Router) { 
  }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.inputRecipes)
    {
      this.recipes = this.inputRecipes;
      console.log(this.inputRecipes);
    }
  }

  filterRecipesSingle(event){
    let query=event.query;
    this.filteredRecipeNames = this.filter(query, this.recipes);
  }

  filter(query, recipes: RecipesListModel[]):any[]{
    let filtered : any[] = [];
    for(let i=0; i < recipes.length; i++)
    {
      let recipe = recipes[i];
      let normalisedName = Diacritics.normaliseString(recipe.Name);
      if(normalisedName.toLowerCase().includes(query.toLowerCase())){
        filtered.push(recipe.Name);
      }
    }
    console.log("filter"+filtered);
    return filtered;
  }

  search(){
    let id: number;
    id = this.recipes.find(recipe => recipe.Name == this.recipeName).Id;
    this.router.navigate(['/recipe-details/' + id]);
  }
}
