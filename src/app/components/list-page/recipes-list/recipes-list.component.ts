import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { RecipesListService } from '../../../services/recipes-list.service';
import { RecipesListModel } from '../../../models/recipes-list.model';
import { Router } from '@angular/router';
import { Ingredients } from '../../../models/ingredients.model';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css'],
  providers: [RecipesListService]
})
export class RecipesListComponent implements OnInit{
  showPaginator: boolean = true;
  public text: string;
  public ingredientName: string[];
  public typedIngredients: string[] = [];
  public typedName:string;

  @Input() recipes: RecipesListModel[];

  constructor(private recipeService: RecipesListService, private router: Router) { }
  ngOnInit() {
  }


  searchWithFilters(event) {
    this.typedIngredients = event;
    this.recipeService.getFilteresRecipesList(this.typedIngredients)
      .subscribe(res => {
        console.log(res);
        this.recipes = res.map(recipe =>
          ({
            Id: recipe.Id,
            Name: recipe.Name,
            Photo: recipe.Photo? 'data:image/png;base64,' + recipe.Photo : null,
            Category: recipe.Category,
            UserId: recipe.UserId,
            RatingValue: recipe.RatingValue
          }));
          this.showPaginator = res.length > 9;
      });
      
  }

}
