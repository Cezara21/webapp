import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RecipesListService } from '../../../services/recipes-list.service';
import { RecipeDetailsModel } from '../../../models/recipe-details.model';
import { RecipeIngredientsModel } from '../../../models/recipe-ingredients.model';
import { UsersService } from '../../../services/users.service';
import { UserFavoriteInfo } from '../../../models/user-favorite-info-model';
import { RatingInfo } from '../../../models/rating-info-model';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css'],
  providers: [RecipesListService]
})
export class RecipeDetailsComponent implements OnInit {
  public value : number = null;
  public userId: any = null;
  public isLoggedIn: boolean = false;
  public isFavorite : boolean = false;
  public recipeDetails: RecipeDetailsModel = new RecipeDetailsModel();
  public recipeIngredients : RecipeIngredientsModel[] = new Array<RecipeIngredientsModel>();
  public selectedId: number;
  public steps : string[];
  public photo : string;
  public cols: any[];
  public userFavorite : UserFavoriteInfo = new UserFavoriteInfo();
  public ratingInfo : RatingInfo = new RatingInfo();
  public userRating: number;
  
  constructor(private recipeService: RecipesListService, private usersService: UsersService, route:ActivatedRoute) {
    console.log("id ruta"+route.snapshot.params.id);
    this.selectedId = route.snapshot.params.id;  
    console.log("id-ul retetei "+this.selectedId);
   }
   
  ngOnInit() {
    this.userId = this.usersService.getUserId();    
    this.loadIngredientsForRecipe();
    this.getRecipeDetails();
    console.log("photo" + this.photo);
    this.isLoggedIn = this.userId != null;
    console.log("is logged in "+this.isLoggedIn);
    if(this.isLoggedIn)
    {
      this.checkUserFavorite();
      this.recipeService.getUserRating(this.userId, this.selectedId).subscribe((res)=>{
        this.userRating = res;
        console.log("user rating "+this.userRating);
      })
    }
  }

  checkUserFavorite(){
    this.recipeService.getUserFavorites(this.userId).subscribe((res)=> {
      if(res.filter(recipe=> recipe.Id == this.selectedId).length > 0){
        this.isFavorite = true;
      }
    });
  }

  loadIngredientsForRecipe(){
    this.recipeService.getRecipeIngredients(this.selectedId).subscribe((res)=>this.recipeIngredients=res);
  }

  getRecipeDetails(): void {
    this.recipeService.getRecipeDetails(this.selectedId).subscribe((res)=>{
      this.recipeDetails=res;
      this.recipeDetails.Steps=res.Directions.split(/[0-9]\./);
      this.recipeDetails.Steps.shift();
      this.steps = this.recipeDetails.Steps;
      if(this.recipeDetails.Photo){
        this.photo='data:image/png;base64,' + this.recipeDetails.Photo;
      }
      this.recipeDetails.RatingValue = res.RatingValue
    });
  }

  addToFavorits(){
    this.userFavorite.RecipeId = this.recipeDetails.Id;
    this.userFavorite.UserId = this.userId;
    this.recipeService.postUserFavorite(this.userFavorite).subscribe((res)=> this.recipeDetails.Id = res);
    this.isFavorite=true;
  }

  deleteFromFavorites(){
    this.recipeService.deleteUserFavorite(this.userId, this.recipeDetails.Id)
    .subscribe((res)=>console.log(res));
    this.isFavorite=false;
  }

  rate(event){
    this.ratingInfo.UserId = this.userId;
    this.ratingInfo.Value = event.value;
    this.ratingInfo.RecipeId = this.recipeDetails.Id;
    this.recipeService.sendRating(this.ratingInfo).subscribe(()=>{});
    this.userRating = event.value;
  }
}
