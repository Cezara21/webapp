import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { RecipeNutrientValue } from '../../../models/recipe-nutrient-value';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnChanges {

  @Input() categoryList : RecipeNutrientValue[];
  public FiveElListsList : RecipeNutrientValue[][] = new Array<Array<RecipeNutrientValue>>();
  private rowLength : number = 5;
  public names : string[] = [];
  public values : number[] = [];
  public measures : string[] = [];

  constructor() { }

  async ngOnChanges(changes: SimpleChanges) {
    if(changes.categoryList)
    {
      this.initializeNames();
    }
  }

  initializeNames() : void{
    this.categoryList.forEach(element=> {
        this.names.push(element.Name);
        this.values.push(element.Value);
        this.measures.push(element.Measure);
    })
  }

  // divideInto5ItemsList(list : RecipeNutrientValue[]) {
  //   const listLength = list.length;
  //   const numberOfLists = list.length/this.rowLength + 1;
  //   var index = 0;
  //   var count = 0;
  //   while(index < numberOfLists){
      
  //     var currentList = list.slice(index, index+this.rowLength);
  //     currentList.forEach(element => {
  //       this.FiveElListsList[count].push(element);
  //     });
      
  //     //this.FiveElListsList.push(currentList);
  //     index += this.rowLength;
  //     count += 1;
  //   }
  //}

}
