import { Component, OnInit, Input } from '@angular/core';
import { RecipeNutrientValue } from '../../../models/recipe-nutrient-value';

@Component({
  selector: 'app-table-part',
  templateUrl: './table-part.component.html',
  styleUrls: ['./table-part.component.css']
})
export class TablePartComponent implements OnInit {
  
  @Input() nutrient: RecipeNutrientValue; 
  public isZero : boolean;
  constructor() { 
    
  }

  ngOnInit() {
    if(this.nutrient.PercentFromDRI == 0)
    {
      this.isZero = true;
    }
  }

}
