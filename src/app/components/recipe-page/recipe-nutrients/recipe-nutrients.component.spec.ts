import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeNutrientsComponent } from './recipe-nutrients.component';

describe('RecipeNutrientsComponent', () => {
  let component: RecipeNutrientsComponent;
  let fixture: ComponentFixture<RecipeNutrientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeNutrientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeNutrientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
