
import { Component, OnInit, Input } from '@angular/core';
import { RecipesListService } from '../../../services/recipes-list.service';

import { RecipeNutrientValue } from '../../../models/recipe-nutrient-value';
import { ActivatedRoute } from '@angular/router';
import { delay } from 'q';

@Component({
  selector: 'app-recipe-nutrients',
  templateUrl: './recipe-nutrients.component.html',
  styleUrls: ['./recipe-nutrients.component.css'],
  providers: [RecipesListService]
})
export class RecipeNutrientsComponent implements OnInit {
  @Input() recipeId : number;

  public recipeNutrientsValues : RecipeNutrientValue[] = [];
  public vitamins : RecipeNutrientValue[] = [];
  public minerals : RecipeNutrientValue[] = [];
  public macronutrients : RecipeNutrientValue[] = [];

  constructor(private recipeService: RecipesListService, route:ActivatedRoute) {
    
   }

  ngOnInit() {
    this.recipeService.getNutrientValues(this.recipeId).subscribe( (res) => {
       this.recipeNutrientsValues = res;
       this.divideNutrientsByCategory();
    });
  } 

  divideNutrientsByCategory(): void {
    this.vitamins = this.recipeNutrientsValues.filter(x=>x.Category=="Vitamine");
    console.log("vitamine" + this.vitamins);
    this.minerals = this.recipeNutrientsValues.filter(x=>x.Category=="Minerale");
    console.log("minerale" + this.minerals);
    this.macronutrients = this.recipeNutrientsValues.filter(x=>x.Category=="Macronutrienți");
  }

  

}