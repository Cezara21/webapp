import { Component, OnInit, ViewChild, ComponentFactoryResolver, ViewContainerRef, OnChanges, SimpleChanges, ElementRef } from '@angular/core';
import { Ingredients } from '../../../models/ingredients.model';
import { RecipesListService } from '../../../services/recipes-list.service';
import { SelectItem, Message } from 'primeng/api';
import { Diacritics } from '../../../helpers/diacritics';
import { CustomIngredientInfo } from '../../../models/custom-ingredient-info.model';
import { AddRecipeInfo } from '../../../models/add-recipe-info.model';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { UsersService } from '../../../services/users.service';
import { Router } from '@angular/router';
import { RecipeDetailsModel } from '../../../models/recipe-details.model';
import { IngredientMeasure } from '../../../models/ingredient-measure-model';
import { Xliff } from '@angular/compiler';


@Component({
  selector: 'app-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.css'],
  providers: [RecipesListService]
})
export class AddRecipeComponent implements OnInit {
  
  public message: Message[] = [];
 
  public recipe: AddRecipeInfo = new AddRecipeInfo();
  public ingredients: string[];
  public categories: SelectItem[] = [];
  public selectedCategory: string = "";

  public errorMessage: string = "Ingredientul nu exista in baza de date";
  public filteredIngredients: string[];
  public measures: SelectItem[] = [];

  public quantity: number = null;
  public selectedIngredient: string = null;
  public selectedMeasure: string = null;
  public selectedFile: File = null;
  public showFile : any =null;

  public invalidCustomIngredient : boolean = false;

  public ingredientMeasures: IngredientMeasure[] = [];
  public ingredientValidMeasures: string;
  public recipeId :number;

  public quantityControl: FormControl;
  public nameControl: FormControl;
  public directionsControl: FormControl;

  constructor(private recipeService: RecipesListService,
     private usersService: UsersService, 
     private fb: FormBuilder,
     private router: Router
    ) {
    this.createForm();
    this.recipeService.getRecipeCategories().subscribe(res => {
      res.forEach(element => {
        this.categories.push({ label: element, value: element })
      });
    })
    this.recipeService.getFullNameIngredients().subscribe(res => {
      this.ingredients = res;
      console.log(this.ingredients);
    });
    this.recipeService.getIngredientsCustomMeasures().subscribe((res)=>{
      this.ingredientMeasures = res;
    })
    this.recipe.UserId = this.usersService.getUserId();
  }

  ngOnInit() {
  }

  createForm(){
    this.quantityControl = new FormControl ('', [Validators.required, Validators.pattern('^[0-9]+$')]);
    this.nameControl = new FormControl('', [Validators.required]);
    this.directionsControl = new FormControl('', [Validators.minLength(30)]);
  }

  filterIngredientsSingle(event) {
    let query = event.query;

    this.filteredIngredients = this.filterIngredients(query, this.ingredients);
    console.log(this.filteredIngredients);
  }

  filterIngredients(query, ingredients: string[]) {
    let filtered: string[] = [];

    for (let i = 0; i < ingredients.length; i++) {
      let ingredient = ingredients[i];
      let normalisedIngredient = Diacritics.normaliseString(ingredient);
      if (normalisedIngredient.includes(query.toLowerCase())) {
        filtered.push(ingredient);
      }
    }
    return filtered;
  }

  remove(ingredient: CustomIngredientInfo) {
    this.recipe.CustomIngredients.splice(this.recipe.CustomIngredients.indexOf(ingredient), 1);
  }

  updateMeasures(){
    let ingredientMeasures =  this.ingredientMeasures.filter(x=> x.IngredientName == this.selectedIngredient);
    let ingredientMeasuresArray = ingredientMeasures.map(x=>x.MeasureName);
    this.measures = [];
    ingredientMeasuresArray.forEach(el => this.measures.push({label: el, value: el}));
    this.measures.push({label:"g", value: "g"});
    console.log("measures"+this.measures);
  }

  addIngredient() {
    if(this.quantity!=null && this.selectedMeasure !=null && this.selectedIngredient!=null)
    {
      if(this.invalidCustomIngredient)
      {
        this.invalidCustomIngredient = false;
        return;
      }
      console.log( this.selectedMeasure + this.quantity + this.selectedIngredient)
      this.recipe.CustomIngredients.push({ Ingredient: this.selectedIngredient, Measure: this.selectedMeasure, Quantity: this.quantity });
      this.selectedMeasure = null;
      this.selectedIngredient = null;
      this.quantity = null;
      this.quantityControl.markAsPristine();
      this.invalidCustomIngredient = false;
    }
    else{
      this.invalidCustomIngredient = true;
    }
    
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);    
    reader.onload = (event:any) => {
      this.showFile = event.target.result;
      var base64result = reader.result.substr(reader.result.indexOf(',') + 1);
      this.recipe.Photo = base64result;
    }
  }

  addRecipe() {
    if(this.checkRecipeValid())
    {
      this.recipe.Category = this.selectedCategory;
      console.log(this.recipe);
      this.recipeService.postRecipe(this.recipe).subscribe(res=> {
        this.recipeId = res,
        this.router.navigate(['recipe-details', this.recipeId]);        
      },
      err => {
        this.showError("A aparut o eroare pe server. Încercați din nou.");
      });
      
      this.clearAllFilters();
    }
    else{
      this.showError("Completați toate câmpurile.");
    }
  }

  checkRecipeValid():boolean{
    return this.recipe.Title!=null && this.selectedCategory!=null && this.recipe.Directions!=null 
    && this.recipe.CustomIngredients.length != 0 ;
  }

  clearAllFilters(){
    this.recipe = new AddRecipeInfo();
    this.selectedMeasure = null;
    this.selectedIngredient = null;
    this.quantity = null;
    this.showFile = null;
    this.markAsPristine();
  }

  markAsPristine(){
    this.quantityControl.markAsPristine();
    this.nameControl.markAsPristine();
    this.directionsControl.markAsPristine();
  }

  showError(message: string) {
    this.message.push({severity:'error', summary:'Eroare', detail: message});
  }

  

}
