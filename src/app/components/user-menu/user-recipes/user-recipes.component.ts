import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users.service';
import { RecipesListService } from '../../../services/recipes-list.service';
import { RecipesListModel } from '../../../models/recipes-list.model';

@Component({
  selector: 'app-user-recipes',
  templateUrl: './user-recipes.component.html',
  styleUrls: ['./user-recipes.component.css'],
  providers: [UsersService, RecipesListService]
})
export class UserRecipesComponent implements OnInit {
  userId: string;
  recipes: RecipesListModel[] = [];
  emptyMessage: string = "Nu ai adăugat nicio rețetă încă";
  constructor(private userService: UsersService, private recipesService: RecipesListService) {
    this.userId = this.userService.getUserId();
  }


  ngOnInit() {
    this.recipesService.getFilteresRecipesList(null).subscribe((res)=>{
      this.recipes = res.filter(x=> x.UserId == this.userId).map(recipe => ({
        Id: recipe.Id,
        Name: recipe.Name,
        Photo: 'data:image/png;base64,' + recipe.Photo,
        Category: recipe.Category, 
        UserId: recipe.UserId,
        RatingValue: recipe.RatingValue
      }));
    });
  }

}
