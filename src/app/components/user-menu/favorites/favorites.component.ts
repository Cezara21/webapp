import { Component, OnInit } from '@angular/core';
import { RecipesListService } from '../../../services/recipes-list.service';
import { UsersService } from '../../../services/users.service';
import { RecipesListModel } from '../../../models/recipes-list.model';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css'],
  providers: [RecipesListService, UsersService]
})

export class FavoritesComponent implements OnInit {
  showDialog: boolean = false;  
  showPaginator : boolean;
  userId: string = null;
  favoriteRecipes : RecipesListModel[] = [];
  recipeId: number = 0;
  emptyMessage: string = "Nu ai adăugat nicio rețetă la favorite încă";
  constructor(private recipeService: RecipesListService, private userService: UsersService) { 
  }

  ngOnInit() {
    this.userId = this.userService.getUserId();
    this.recipeService.getUserFavorites(this.userId).subscribe( (res) => {
      this.favoriteRecipes = res.map(recipe =>
        ({
          Id: recipe.Id,
          Name: recipe.Name,
          Photo: 'data:image/png;base64,' + recipe.Photo,
          Category: recipe.Category, 
          UserId: recipe.UserId,
          RatingValue: recipe.RatingValue
        }));
      console.log(this.favoriteRecipes);
    })
    this.showPaginator = this.favoriteRecipes.length > 9;
  }


    enableShowDialog(recipeId: number) {
        this.showDialog = true;
        this.recipeId = recipeId;
    }

    delete(){
      this.recipeService.deleteUserFavorite(this.userId, this.recipeId)
      .subscribe((res)=>console.log(res));
      let index = this.favoriteRecipes.findIndex(x=> x.Id == this.recipeId);
      this.favoriteRecipes.splice(index, 1);
      this.showDialog = false;
    }

    no(){
      this.showDialog = false;
    }
}
