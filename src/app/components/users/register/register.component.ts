import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../../../services/users.service';
import { User } from '../../../models/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UsersService]
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private fb: FormBuilder, private userService: UsersService, private router: Router) {
    this.createForm();
  }

  ngOnInit(): void {}

  createForm() {
    this.registerForm = this.fb.group({
      firstName: ['', [Validators.required,  Validators.minLength(3)]],
      lastName: ['', [Validators.required,  Validators.minLength(3)]],
      email: ['', Validators.email],
      passwords: this.fb.group({
        password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('.*[0-9].*')]],
        repeatPassword: ['', [Validators.required, Validators.minLength(6), Validators.pattern('.*[0-9].*')]],
    }, {validator: this.passwordConfirming})
    });
  }

  passwordConfirming(c: FormGroup) {
    if (c.controls['repeatPassword'].value === c.controls['password'].value) {
        return null;
    }
    return {missmatch: true};
  }

  onSubmit() {
    if(this.registerForm.valid)
    {
      var userModel: User = { 
        Email: this.registerForm.controls['email'].value,
        Password:  this.registerForm.get('passwords').get('password').value,
        FirstName: this.registerForm.controls['firstName'].value,
        LastName: this.registerForm.controls['lastName'].value
    };

      this.userService.register(userModel).subscribe(
        (data) => {
          this.router.navigate(['/recipe-list']);
        },
        (error: Error) => {
          console.log("error", error);
        },
        () => {}
      );
    }

  }
}
