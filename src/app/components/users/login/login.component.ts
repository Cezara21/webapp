import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../../../services/users.service';
import { Router } from '@angular/router';
import { HttpEventType } from '@angular/common/http';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../register/register.component.css'],
  providers: [ UsersService ]
})
export class LoginComponent implements OnInit {
  isValid: boolean = true;
  registerForm: FormGroup;

  constructor(private fb: FormBuilder, private userService: UsersService, private router: Router) { 
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    })
  }

  onSubmit(){
    if(this.registerForm.valid){
        var email = this.registerForm.controls['email'].value;
        var password = this.registerForm.get('password').value;

        this.userService.login(email, password).subscribe(res => {
          console.log("login raspuns "+res);
          // login successful if there's a jwt token in the response
          if (res && res.Token) {
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem('currentUser', JSON.stringify(res));//aici se pune ce vine din backend
              console.log("User logged in");
              console.log(localStorage.getItem('currentUser'));
              this.router.navigate(['/recipe-list']);     
              this.isValid = true;  
          }
          
      }, err => {
        this.isValid = false;
        console.log("invalid");
      })
    }
}

}
