import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UsersService } from './services/users.service';

@Injectable()
export class UserloggedInGuardGuard implements CanActivate {

  constructor(
    private userService: UsersService,
    private router: Router
  ) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('currentUser')) {
      console.log(localStorage.getItem('currentUser'));
        // logged in so return true
        return true;
    }
    console.log(localStorage.getItem('currentUser'));

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    return false;
  }
}